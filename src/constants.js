export const SERVICES = [
  {
    value: 1,
    text: 'Еда и продукты'
  },
  {
    value: 2,
    text: 'Доставка'
  },
  {
    value: 3,
    text: 'Медецина'
  },
  {
    value: 4,
    text: 'Хобби и развлечения'
  },
  {
    value: 5,
    text: 'Одежда, обувь, аксессуары'
  },
  {
    value: 6,
    text: 'Цветы и подарки'
  },
  {
    value: 7,
    text: 'Обучение'
  },
  {
    value: 8,
    text: 'Авто'
  },
  {
    value: 9,
    text: 'Прочее'
  }
]

export const DIAPASONS = [100, 500, 1000, 5000]
