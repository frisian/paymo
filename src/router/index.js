import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Index from "@/views/Index/Index";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Index
  },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
